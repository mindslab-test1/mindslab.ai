#!/bin/bash

NAME="maumchatbot"                                     # Name of the application (*)
DJANGODIR=${HOME}/maumchatbot/maumchatbot  # Django project directory (*)
SOCKFILE=${DJANGODIR}/gunicorn/gunicorn.sock        # We will communicate using this unix socket (*)

USER=maum     # the user to run as (*)
GROUP=maum    # the group to run as (*)
NUM_CORES=`cat /proc/cpuinfo | grep processor | wc -l`
NUM_WORKERS=4 # how many worker processes should Gunicorn spawn (*)
DJANGO_SETTINGS_MODULE=maumchatbot.settings    # which settings file should Django use (*)
DJANGO_WSGI_MODULE=maumchatbot.wsgi           # WSGI module name (*)

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
source ${DJANGODIR}/venv/bin/activate
cd $DJANGODIR
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
#export PYTHONPATH=$DJANGODIR${PYTHONPATH:+:}${PYTHONPATH:-}
export PYTHONPATH=/home/maum/maumchatbot/maumchatbot/venv/bin/python
echo $PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR
echo $RUNDIR
# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
  --limit-request-line 8190\
  --name $NAME \
  --workers $NUM_WORKERS \
  --user $USER \
  --timeout 5000 \
  --bind=unix:$SOCKFILE \
  --access-logfile ${DJANGODIR}/gunicorn/gunicorn-access.log \
  --error-logfile  ${DJANGODIR}/gunicorn/gunicorn-error.log


  ##-b 0:8001 \
