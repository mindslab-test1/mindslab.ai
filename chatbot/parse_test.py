from bs4 import BeautifulSoup
from urllib.request import urlopen


# html = urlopen("https://www.youtube.com/watch?v=761ae_KDg_Q")
# source = html.read()
# html.close()
#
# soup = BeautifulSoup(source, "html.parser")
# title = soup.find("title").getText()
# print(title)

# test_text = "안녕하세요 잘가요. 하나도 재미가 없네요."
# test_text = "$$URL_S$$https://www.youtube.com/watch?v=dBD54EZIrZo$$URL_F$$$$HB_S$$책 읽어줘,노래 불러줘$$HB_F$$"
# test_text = "안녕하세요 URL 예시2 입니다. 복수 URL이면 썸네일이 안나와요. $$URL_S$$https://www.naver.com/$$URL_F$$ 중간에 다시 텍스트가 나오고 그다음에 다시 URL를 뿌려주는 예시에요. $$URL_S$$https://www.naver.com/$$URL_F$$"
test_text = "안녕하세요 이미지 예시입니다. $$IMG_S$$https://file.okky.kr/images/1513127807083.jpg$$IMG_F$$"


def parse_v_btn(test_text):
    temp_text = test_text.split("$$VB_S$$")

    if len(temp_text) == 1:
        return test_text

    h_btn_text = temp_text[1].split("$$VB_F$$")[0]

    result_text = temp_text[0] + temp_text[1].split("$$VB_F$$")[1]

    h_btn_list = h_btn_text.split(',')

    mid_text = ''
    for item in h_btn_list:
        # print(item)
        mid_text += """<li><a href="javascript:btnTextToTalk('%s')">%s</a></li>""" % (item, item)

    h_btn_html = """
        <li class="botMsg_btnLst">
            <ul>
                %s
            </ul>
        </li>
    """ % mid_text

    # print(result_text)
    return {
        "text": result_text,
        "html": h_btn_html
    }


def add_new_line(test_text):
    result_text = test_text.replace("$$NL$$", "<br/>")
    return result_text


def add_anchor_tag(test_text):
    temp_text = test_text.split("$$URL_S$$")
    if len(temp_text) == 1:
        return test_text

    result_text = ''
    for item in temp_text:
        # print(item)
        url_text = item.split("$$URL_F$$")[0]
        html_text = """<a href="%s" target="_blank">%s</a>""" % (url_text, url_text)
        result_text = test_text.replace('$$URL_S$$' + url_text + '$$URL_F$$', html_text)
        # print(result_text)
    return result_text


def parse_img(test_text):
    temp_text = test_text.split("$$IMG_S$$")

    if len(temp_text) == 1:
        return test_text

    img_text = temp_text[1].split("$$IMG_F$$")[0]
    img_html = """<img src='%s'>""" % img_text

    result_text = temp_text[0] + temp_text[1].split("$$IMG_F$$")[1]

    return {
        "text": result_text,
        "html": img_html
    }


print(parse_img(test_text))
