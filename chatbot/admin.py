from django.contrib import admin
from .models import PR_Press,PR_Video,Recruitment


class PressAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['id', 'title','image','image_url', 'content', 'created_date']
    list_display_links = ['id','title']
    ordering = ('-created_date',)


class VideoAdmin(admin.ModelAdmin):
    list_per_page = 9
    list_display = ['id', 'title', 'video_url','category']
    list_display_links = ['id', 'title']
    ordering = ('category',)


class RecruitAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['id', 'title', 'image', 'content', 'created_date']
    list_display_links = ['id', 'title']
    ordering = ('-created_date',)



admin.site.register(PR_Press, PressAdmin)
admin.site.register(PR_Video, VideoAdmin)
admin.site.register(Recruitment, RecruitAdmin)