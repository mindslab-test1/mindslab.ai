from bs4 import BeautifulSoup
from urllib.request import urlopen
import smtplib
from email.mime.text import MIMEText
import maumchatbot.email_settings as email_setting

class ChatbotService:
    def send_gmail(self, mail_to, mail_body, mail_subject):
        # msg = MIMEText(email_text, "html", _charset="utf-8")
        msg = MIMEText(mail_body)
        msg['Subject'] = mail_subject
        msg['From'] = email_setting.GMAIL_USER
        msg['To'] = mail_to

        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(email_setting.GMAIL_USER, email_setting.GMAIL_PASSWORD)

        server.sendmail(email_setting.GMAIL_USER, mail_to, msg.as_string())
        server.close()

        print('Email was sent to %s!', mail_to)

    def make_botchat(self, utter_text):
        #print(utter_text)

        # 1. 가로 버튼 생성 및 html 태그 입력
        h_btn = self.parse_h_btn(utter_text)
        return_obj = {
            "h_btn": h_btn['html']
        }

        # 2. 세로 버튼 생성 및 html 태그 입력
        v_btn = self.parse_v_btn(h_btn['text'])
        return_obj['v_btn'] = v_btn['html']

        # 3. 이미지 URL 을 img 태그를 이용해서 화면에 보여주기
        img = self.parse_img(v_btn['text'])
        return_obj['image'] = img['html']

        # 4. URL 파싱 및 html 태그 입력
        a_text = self.add_anchor_tag(img['text'])

        # 5. URL 로 og meta 태그 데이터 파싱
        # print(a_text['urls'])
        if len(a_text['urls']) > 0 and a_text['urls'][0] != '':
            # youtube url 따로 처리
            if ("//www.youtube.com" or "//youtube.com" or "//youtu.be") in a_text['urls'][0]:
                if ("/channel") in a_text['urls'][0]:
                    thumbnail_tag = self.parse_youtube_meta("https://www.youtube.com/watch?v=ACFFt58X-tE") #sully : 회사소개 영상 "계정"인 경우 썸네일 이미지를 보여주기 위해 url하드코딩
                else:
                    thumbnail_tag = self.parse_youtube_meta(a_text['urls'][0])
            else:
                thumbnail_tag = self.parse_og_meta(a_text['urls'][0])
            return_obj['thumbnail'] = thumbnail_tag

        # 7. 줄바꿈 문자 처리
        return_obj['utter'] = self.add_new_line(a_text['text'])
        # 8. email 태그 만들기
        return_obj['utter'] = self.make_mailto_link(return_obj['utter'])['text']
        return return_obj

    def make_mailto_link(self, test_text):
        temp_text = test_text.split("$$MAIL_S$$")

        if len(temp_text) == 1:
            return {
                "text": test_text,
                "html": ""
            }

        email_text = temp_text[1].split("$$MAIL_F$$")[0]
        # print(email_text)

        email_html = """
                    <a href="mailto:%s">%s</a>
                    """ % (email_text, email_text)
        result_text = temp_text[0] + email_html + temp_text[1].split("$$MAIL_F$$")[1]
        # print(email_html)

        return {
            "text": result_text,
            "html": email_html
        }

    def parse_img(self, test_text):
        temp_text = test_text.split("$$IMG_S$$")

        if len(temp_text) == 1:
            return {
                "text": test_text,
                "html": ""
            }

        img_text = temp_text[1].split("$$IMG_F$$")[0]
        img_html = """
            <div class="imgBox">
                <img src='%s'>
            </div>
            """ % img_text

        result_text = temp_text[0] + temp_text[1].split("$$IMG_F$$")[1]

        return {
            "text": result_text,
            "html": img_html
        }

    def add_anchor_tag(self, test_text):
        if '$$URL_RS$$' in test_text:
            temp_text = test_text.split("$$URL_RS$$")
            if len(temp_text) == 1:
                return {
                    "text": test_text,
                    "urls": ""
                }

            result_text = ''
            urls = []
            for item in temp_text:
                # print(item)
                if len(item.split("$$URL_RF$$")) != 1:
                    url_text = item.split("$$URL_RF$$")[0]
                    urls.append(url_text)
                    html_text = """<a href="%s" target="_blank">▶ 결과보기</a>""" % (url_text)
                    result_text = test_text.replace('$$URL_RS$$' + url_text + '$$URL_RF$$', html_text)
                    # print(result_text)
            return {
                "text": result_text,
                "urls": urls
            }
        else:
            temp_text = test_text.split("$$URL_S$$")
            if len(temp_text) == 1:
                return {
                    "text": test_text,
                    "urls": ""
                }

            result_text = ''
            urls = []
            for item in temp_text:
                # print(item)
                if len(item.split("$$URL_F$$")) != 1:
                    url_text = item.split("$$URL_F$$")[0]
                    urls.append(url_text)
                    html_text = """<a href="%s" target="_blank">%s</a>""" % (url_text, url_text)
                    result_text = test_text.replace('$$URL_S$$' + url_text + '$$URL_F$$', html_text)
                    #print(result_text)
                    #print(urls)

            return {
                "text": result_text,
                "urls": urls
            }


    def add_new_line(self, test_text):
        result_text = test_text.replace("$$NL$$", "<br/>")
        return result_text

    # return text , v_btn html tag
    def parse_v_btn(self, utter_text):
        temp_text = utter_text.split("$$VB_S$$")
        if len(temp_text) == 1:
            return {
                "text": utter_text,
                "html": ""
            }

        h_btn_text = temp_text[1].split("$$VB_F$$")[0]

        result_text = temp_text[0] + temp_text[1].split("$$VB_F$$")[1]

        h_btn_list = h_btn_text.split(',')

        mid_text = ''
        for item in h_btn_list:
            # print(item)
            mid_text += """<li><a href="javascript:btnTextToTalk('%s')">%s</a></li>""" % (item, item)

        h_btn_html = """
            <li class="botMsg_btnLst">
                <ul>
                    %s
                </ul>
            </li>
        """ % mid_text

        # print(result_text)
        return {
            "text": result_text,
            "html": h_btn_html
        }

    # return text , h_btn html tag
    def parse_h_btn(self, utter_text):
        temp_text = utter_text.split("$$HB_S$$")
        if len(temp_text) == 1:
            return {
                "text": utter_text,
                "html": ""
            }

        h_btn_text = temp_text[1].split("$$HB_F$$")[0]

        result_text = temp_text[0] + temp_text[1].split("$$HB_F$$")[1]

        h_btn_list = h_btn_text.split(',')

        mid_text = ''
        for item in h_btn_list:
            # print(item)
            mid_text += """<li><a href="javascript:btnTextToTalk('%s')">%s</a></li>""" % (item, item)

        h_btn_html = """
            <li class="botMsg_btnItem">
                <ul>
                    %s
                </ul>
            </li>
        """ % mid_text

        # print(result_text)
        return {
            "text": result_text,
            "html": h_btn_html
        }

    def parse_youtube_meta(self, url):
        html = urlopen(url)
        source = html.read()
        html.close()

        # url example "https://youtu.be/0XsCKrb3gt4"
        # "https://www.youtube.com/watch?v=761ae_KDg_Q"

        # if url has playlist do not make thumbnail HTML
        if "playlist" in url:
            return ""

        soup = BeautifulSoup(source, "html.parser")
        ori_url = url.split("=")[-1]
        ori_url = ori_url.split("=")[-1]
        if ori_url is None:
            video_code = url.split("/")[-1]
        else:
            video_code = ori_url
        image = "https://img.youtube.com/vi/" + video_code + "/sddefault.jpg"
        title = soup.find("title").getText()

        result = """
        <li class="botMsg_generic">
            <span class="generic_image"><img src="%s"></span>
            <span class="generic_url"><a href="%s" target="_blank"><em class="generic_title">%s</em></a></span>
            <span class="generic_description">%s</span>
        </li>
		""" % (image, url, title, "")
        return result

    def parse_og_meta(self, url):
        #print(url)
        if 'localhost' in url:
            return ""
        if 'https://maum.ai/' in url:
            return ""
        html = urlopen(url)
        source = html.read()
        html.close()

        soup = BeautifulSoup(source, "html.parser")
        og_title = soup.find("meta", property="og:title")
        og_url = soup.find("meta", property="og:url")
        og_image = soup.find("meta", property="og:image")
        og_desc = soup.find("meta", property="og:description")

        if og_title is None:
            og_title = {"content": None}
            return ""
        # if og_url is None:
        #     og_url = {"content": None}
        if og_image is None:
            og_image = {"content": None}
            return ""
        if og_desc is None:
            og_desc = {"content": None}
            return ""

        if og_title['content'] is "":
            return ""
        if og_image['content'] is "":
            return ""
        if og_desc['content'] is "":
            return ""

        result = """
            <li class="botMsg_generic">
                <span class="generic_image"><img src="%s"></span>
                <span class="generic_url"><a href="%s" target="_blank"><em class="generic_title">%s</em></a></span>
                <span class="generic_description">%s</span>
            </li>
            """ % (og_image['content'], url, og_title['content'], og_desc['content'])
        return result


if __name__ == "__main__":
    # test_text = "안녕하세요 잘가요. 하나도 재미가 없네요."
    # test_text = "$$URL_S$$https://www.youtube.com/watch?v=dBD54EZIrZo$$URL_F$$$$HB_S$$책 읽어줘,노래 불러줘$$HB_F$$"
    # test_text = "안녕하세요 URL 예시2 입니다. 복수 URL이면 썸네일이 안나와요. $$URL_S$$https://www.naver.com/$$URL_F$$ 중간에 다시 텍스트가 나오고 그다음에 다시 URL를 뿌려주는 예시에요. $$URL_S$$https://www.naver.com/$$URL_F$$"
    # test_text = "안녕하세요 이미지 예시입니다. $$IMG_S$$https://file.okky.kr/images/1513127807083.jpg$$IMG_F$$"
    #test_text = "현재 공채를 진행하지 않더라도, 마인즈랩의 문은 언제나 열려있으니, 마인즈랩에 입사를 희망하시는 분은 $$URL_S$$aidan@mindslab.ai $$URL_F$$ 로 입사지원서(자유양식)를 보내주시면 검토하여 답장드리겠습니다.$$HB_S$$위치,소개서,홍보영상$$HB_F$$"
#     test_text = """
#         마인즈랩(MINDs Lab)은
#  경기도 성남시 분당구 대왕판교로
# 644번길 49, 다산타워 601호에 위치하고 있습니다.
#
# - 지하철 이용시
# 판교 테크노밸리역 1번 출구
#  - 광역버스 이용시
# 9401번, 9007번, 1002번 등$$URL_S$$https://goo.gl/maps/HXqMn3puXiP2$$URL_F$$$$HB_S$$위치,소개서,홍보영상$$HB_F$$"""

    test_text = "현재 공채를 진행하지 않더라도, 마인즈랩의 문은 언제나 열려있으니, 마인즈랩에 입사를 희망하시는 분은 $$MAIL_S$$aidan@mindslab.ai $$MAIL_F$$ 로 입사지원서(자유양식)를 보내주시면 검토하여 답장드리겠습니다.$$HB_S$$위치,소개서,홍보영상$$HB_F$$"

    chatbot = ChatbotService()
    test = chatbot.make_botchat(test_text)
    # test = chatbot.make_mailto_link(test_text)
    print(test)
