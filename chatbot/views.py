import time

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse
from django.shortcuts import render

import maumchatbot.email_settings as email_setting
from .models import PR_Press, PR_Video, Recruitment
from .service import ChatbotService


# Create your views here.
# from django.http import HttpResponse


def index(request):
    return render(request, 'index.html')


def process_utter(request):
    utter_text = request.GET['utter_text']
    bot_chat = ChatbotService()
    result = bot_chat.make_botchat(utter_text)
    return JsonResponse(result)


def en_index(request):
    return render(request, 'en/index03.html', {'timeStamp': getTime()})


def en_contact_us_page(request):
    return render(request, 'en/contact_us_page.html')


def en_acc(request):
    return render(request, 'en/acc.html', {'timeStamp': getTime()})


def en_portfolio(request):
    return render(request, 'en/portfolio.html', {'timeStamp': getTime()})


def en_floating_sully(request):
    return render(request, 'en/bot_sully_floating.html')


def en_pricing(request):
    return render(request, 'en/pricing.html')


def en_company(request):
    return render(request, 'en/company.html', {'timeStamp': getTime()})


def kr_index(request):
    return render(request, 'kr/index03.html')


def kr_maum_chatbot(request):
    return render(request, 'kr/index03.html', {'timeStamp': getTime()})


def kr_contact_us_page(request):
    return render(request, 'kr/contact_us_page.html')


def kr_acc(request):
    return render(request, 'kr/acc.html', {'timeStamp': getTime()})


def kr_portfolio(request):
    return render(request, 'kr/portfolio.html', {'timeStamp': getTime()})


def kr_floating_sully(request):
    return render(request, 'kr/bot_sully_floating.html')


def kr_pricing(request):
    return render(request, 'kr/pricing.html')


def kr_product(request):
    return render(request, 'kr/index04.html')


# def kr_company(request):
#     return render(request, 'kr/company.html', {'timeStamp': getTime()})


def kr_company(request):
    #### Press ####
    press_list = PR_Press.objects.all().order_by('-created_date')
    total_press = press_list.count()
    pr_page = request.GET.get('page', 1)
    pr_paginator = Paginator(press_list, 10)
    press = pr_paginator.get_page(pr_page)
    try:
        users = pr_paginator.page(pr_page)
    except PageNotAnInteger:
        users = pr_paginator.page(1)
    except EmptyPage:
        users = pr_paginator.page(pr_paginator.num_pages)

    #### Video1 ####
    video1_list = PR_Video.objects.filter(category='시연영상').order_by('id')
    total_video1 = video1_list.count()
    video1_page = request.GET.get('page', 1)
    video1_paginator = Paginator(video1_list, 6)
    video1 = video1_paginator.get_page(video1_page)
    try:
        users = video1_paginator.page(video1_page)
    except PageNotAnInteger:
        users = video1_paginator.page(1)
    except EmptyPage:
        users = video1_paginator.page(video1_paginator.num_pages)

    #### Video ####
    video2_list = PR_Video.objects.filter(category='홍보영상').order_by('id')
    total_video2 = video2_list.count()
    video2_page = request.GET.get('page', 1)
    video2_paginator = Paginator(video2_list, 6)
    video2 = video2_paginator.get_page(video2_page)
    try:
        users = video2_paginator.page(video2_page)
    except PageNotAnInteger:
        users = video2_paginator.page(1)
    except EmptyPage:
        users = video2_paginator.page(video2_paginator.num_pages)

    #### Recruitment ####
    # recru_list = Recruitment.objects.all().order_by('-created_date')
    # total_recruit = recru_list.count()
    # recru_page = request.GET.get('page',1)
    # recru_paginator = Paginator(recru_list,10)
    # recru = recru_paginator.get_page(recru_page)
    # try:
    #     users = recru_paginator.page(recru_page)
    # except PageNotAnInteger:
    #     users = recru_paginator.page(1)
    # except EmptyPage:
    #     users = recru_paginator.page(recru_paginator.num_pages)
    #
    # return render(request, 'company.html',{'press':press, 'total_press': total_press,'video1':video1, 'total_video1': total_video1, 'video2':video2, 'total_video2': total_video2,'recruitment':recru, 'total_recruit': total_recruit})

    recru_list = Recruitment.objects.all().order_by('-created_date')
    lst = []
    i = 0
    for item in recru_list:
        i += 1
        lst.append(
            {'number': i, 'id': item.id, 'title': item.title, 'created_date': item.created_date.strftime('%Y-%m-%d')})

    return render(request, 'kr/company.html',
                  {'press': press, 'total_press': total_press, 'video1': video1, 'total_video1': total_video1,
                   'video2': video2, 'total_video2': total_video2, 'recru_list': lst, 'timeStamp': getTime()})


def kr_press_list(request):
    pageNum = int(request.POST['pageNum'])
    tot_count = int(request.POST['tot_count'])
    modelName = request.POST['modelName']
    lst = []
    i = 0
    obj = None
    start = (pageNum - 1) * 10

    if tot_count < (pageNum * 10):
        end = tot_count
    else:
        end = pageNum * 10

    if modelName == "PR_Press":
        obj = PR_Press.objects.all().order_by('-created_date')[start:end]
    elif modelName == "Recruitment":
        obj = Recruitment.objects.all().order_by('-created_date')[start:end]

    for item in obj:
        num = (tot_count - (pageNum - 1) * 10) - i
        lst.append(
            {'number': num, 'id': item.id, 'title': item.title, 'created_date': item.created_date.strftime('%Y-%m-%d')})
        i += 1
    data = {'lists': lst}
    return JsonResponse(data)


def kr_video_list(request):
    pageNum = int(request.POST['pageNum'])
    tot_count = int(request.POST['tot_count'])
    category = request.POST['category']
    lst = []
    i = 0
    start = (pageNum - 1) * 6
    if tot_count < (pageNum * 6):
        end = tot_count
    else:
        end = pageNum * 6
    video = PR_Video.objects.filter(category=category).order_by('-id')[start:end]

    for item in video:
        lst.append({'title': item.title, 'video_url': item.video_url, 'category': item.category})
        i += 1
    data = {'lists': lst}
    return JsonResponse(data)


def kr_press_detail(request):
    id = int(request.POST['id'])
    press = PR_Press.objects.get(id=id)

    if press.image_url is not None:
        image = press.image_url
    else:
        if press.image == "":
            image = "None"
        else:
            image = press.image.path
            image = image.replace("\\", "/")
            image = "../" + image[image.find("media"):]
    data = {'image': image, 'content': press.content}
    return JsonResponse(data)


def kr_recruit_detail(request):
    id = int(request.POST['id'])
    recruit = Recruitment.objects.get(id=id)

    if recruit.image == "":
        image = "None"
    else:
        image = recruit.image.path
        image = image.replace("\\", "/")
        image = "../" + image[image.find("media"):]
    data = {'image': image, 'content': recruit.content}
    return JsonResponse(data)


def post_contact_us(request):
    # print("post_contact_us"+request.POST)
    # email, name, company, department, phone
    route = request.POST['route']
    email = request.POST['email']
    name = request.POST['name']
    phone = request.POST['phone']
    contact = request.POST['contact']
    mail_to = email_setting.CONTACT_US
    mail_subject = 'Contact Us 메일발송'
    mail_body = """
    %s
    담당자명: %s 
    담당자 이메일: %s  
    담당자 전화번호: %s 
    문의내용: %s 
    """ % (route, name, email, phone, contact)
    send_mail = ChatbotService()
    send_mail.send_gmail(mail_to, mail_body, mail_subject)

    print(email)
    return JsonResponse({"status": "success"})


def send_email(request):
    # email, name, company, department, phone
    email = request.GET['email']
    name = request.GET['name']
    company = request.GET['company']
    department = request.GET['department']
    phone = request.GET['phone']
    mail_to = email_setting.MAIL_TO
    mail_subject = '무료 챗봇 신청서'
    mail_body = """
    메일 발송경로: maum.ai/event 챗봇 신청서
    담당자명: %s 
    기업명: %s 
    담당부서명: %s 
    담당자 전화번호: %s 
    담당자 이메일: %s  
    """ % (name, company, department, phone, email)
    send_mail = ChatbotService()
    send_mail.send_gmail(mail_to, mail_body, mail_subject)

    print(email)
    return JsonResponse({})


def contact_us(request):
    # email, name, company, department, phone
    email = request.GET['email']
    name = request.GET['name']
    phone = request.GET['phone']
    contact = request.GET['contact']
    mail_to = email_setting.CONTACT_US
    mail_subject = 'Contact Us 메일발송'
    mail_body = """
    담당자명: %s 
    담당자 이메일: %s  
    담당자 전화번호: %s 
    문의내용: %s 
    """ % (name, email, phone, contact)
    send_mail = ChatbotService()
    send_mail.send_gmail(mail_to, mail_body, mail_subject)

    print(email)
    return JsonResponse({})


# e-book 발송용
def send_ebook_info(request):
    ## send to 'hello account'
    route = request.POST['route']
    name = request.POST['name']
    company = request.POST['company']
    email = request.POST['email']
    phone = request.POST['phone']
    mail_to = email_setting.MAIL_TO
    mail_subject = 'MindsLab e-book 다운로드'
    hello_mail_body = """
    메일 발송경로: %s
    이름: %s
    회사(소속): %s
    e-mail: %s 
    연락처: %s  
    """ % (route, name, company, email, phone)
    send_mail = ChatbotService()
    send_mail.send_gmail(mail_to, hello_mail_body, mail_subject)

    ## send to client
    # client_mail_body = """
    # 블라블라
    # e-book 다운로드: %s
    # """ % ("https://"+request.get_host()+"/ebook_dwn_link")
    # send_mail.send_gmail(email, client_mail_body, mail_subject)

    print(email)
    return JsonResponse({})


# css 캐시 이슈 해결용
def getTime():
    return time.time()


# 세미나 발표자료 download
def seminar_dwn(request):
    return render(request, 'kr/seminar_dwn.html')

# def ebook_dwn_link(request):
#     filePath = os.path.join(STATIC_ROOT,'chatbot/pdf/MINDsLab.pdf')
#     print(filePath)
#     with open(filePath, 'rb') as file:
#         response = HttpResponse(file, content_type='application/pdf')
#         response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(filePath)
#         return response
