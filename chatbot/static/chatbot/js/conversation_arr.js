//질문 리스트
var qstn1 = ["How many people is the reservation for?", "What day would you like to visit?", "What time would you prefer?", "Thank you. Please give me your name."];
var qstn2 = ["Would you like everything on it?", "Could I get you something to drink?", "Would you like anything else?", "Thank you. Please wait a moment."];
var qstn3 = ["That’s good. What time would like to meet?", "I'm okay. Where would you like to meet?", "How many more people will be there with you?", "I see. I'll contact you soon. Thanks."];
var qstn = [qstn1, qstn2, qstn3];

//Tips 리스트
var arr1_1 = "<li>\
	<p>핵심표현</p>\
	<em>would like to / want to<span>~하고싶다</span></em>\
	<em>make a reservation<span>예약하다</span></em>\
	<em>reserve a table<span>테이블을 예약하다</span></em>\
	<em>book a table<span>테이블을 예약하다</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>I want to reserve a table.</em>\
	<em>I would like to make a reservation.</em>\
</li>";

var arr1_2 = "<li>\
	<p>핵심표현</p>\
	<em>It's for (숫자) of us.<span>~ 명요.</span></em>\
	<em>There will be (숫자) of us.<span>~ 명이 올 거예요.</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>It's for two of us.</em>\
	<em>There will be eight of us.</em>\
</li>";

var arr1_3 = "<li>\
	<p>핵심표현</p>\
	<em>would like to / want<span>~하고 싶다</span></em>\
	<em>this (요일)<span>이번 주 ~</span></em>\
	<em>next (요일)<span>다음 주 ~</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>We'd like to visit this Monday.</em>\
	<em>We want to visit this Thursday.</em>\
</li>";

var arr1_4 = "<li>\
	<p>핵심표현</p>\
	<em>prefer<span>선호하다</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>We prefer three.</em>\
	<em>Six-thirty.</em>\
</li>";

var arr2_1 = "<li>\
	<p>핵심표현</p>\
	<em>would like to / want to<span>~하고싶다</span></em>\
	<em>have/eat<span>먹다</span></em>\
	<em>hamburger<span>햄버거</span></em>\
	<em>cheeseburger<span>치즈 버거</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>Yes, I'd like to have a hamburger.</em>\
	<em>Yes, I would like to have a hamburger.</em>\
</li>";

var arr2_2 = "<li>\
	<p>핵심표현</p>\
	<em>would like to / want to<span>~하고싶다</span></em>\
	<em>everything<span>전부</span></em>\
	<em>except<span>~을 빼고</span></em>\
	<em>tomatoes<span>토마토</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>Yes, I would like everything on it.</em>\
	<em>Everything except onions.</em>\
</li>";

var arr2_3 = "<li>\
	<p>핵심표현</p>\
	<em>would like to / want to<span>~하고싶다</span></em>\
	<em>coke<span>콜라</span></em>\
	<em>orange juice<span>오렌지 주스</span></em>\
	<em>milk shake<span>밀크 셰이크</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>Yes, I would like to drink a coke.</em>\
	<em>Yes, I'd like a coke.</em>\
</li>";

var arr2_4 = "<li>\
	<p>핵심표현</p>\
	<em>would like to / want to<span>~하고싶다</span></em>\
	<em>French fries<span>감자 튀김</span></em>\
	<em>ice cream<span>아이스크림</span></em>\
	<em>chicken nuggets<span>치킨 너겟</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>Yes, I'd like some French fries.</em>\
	<em>Yes, I want some French fry.</em>\
</li>";

var arr3_1 = "<li>\
	<p>핵심표현</p>\
	<em>available<span>시간이 있는</span></em>\
	<em>have time<span>시간이 있다</span></em>\
	<em>How about/What about ~?<span>~은 어때요?</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>I'm available this Monday.</em>\
	<em>I have time this Sunday.</em>\
</li>";

var arr3_2 = "<li>\
	<p>핵심표현</p>\
	<em>would like to / want<span>~하고 싶다</span></em>\
	<em>How about/What about ~?<span>~은 어때요?</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>I'd like to meet at two.</em>\
	<em>At seven-thirty.</em>\
</li>";

var arr3_3 = "<li>\
	<p>핵심표현</p>\
	<em>would like to / want<span>~하고 싶다</span></em>\
	<em>How about/What about ~?<span>~은 어때요?</span></em>\
	<em>office<span>사무실</span></em>\
	<em>restaurant<span>레스토랑</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>I'd like to meet in my office.</em>\
	<em>At restaurant.</em>\
</li>";

var arr3_4 = "<li>\
	<p>핵심표현</p>\
	<em>There will be (around) (숫자) more.<span>(약) ~ 명이 더 올 거예요.</span></em>\
</li>\
<li>\
	<p>대표 답변 문장</p>\
	<em>There will be one more.</em>\
	<em>Around four more.</em>\
</li>";

var arra = [[arr1_1, arr1_2, arr1_3, arr1_4], [arr2_1, arr2_2, arr2_3, arr2_4], [arr3_1, arr3_2, arr3_3, arr3_4]];