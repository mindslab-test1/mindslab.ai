/*License (MIT)

Copyright © 2013 Matt Diamond

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.
*/
"use strict"
var recLength = 0,
    recBuffersL = [],
    recBuffersR = [],
    sampleRate;

var webSocket;
var isConnected = false;

function encodeStreamWav(samples, mono) {
    var buffer = new ArrayBuffer(samples.length * 2);
    var view = new DataView(buffer);
    floatTo16BitPCM(view, 0, samples);
    return view;
}

function sendMessage(pcm){
    if(isConnected == false) return;
    var _buf = downsampleBuffer(pcm, 16000);
//    console.log( "Send to Server => pcm data:");
    var view = encodeStreamWav(_buf, true);
    webSocket.send(view);
//    var audioBlob = new Blob([view], { type: 'audio/wav' });
//
//    var reader = new FileReader();
//    var base64data;
//    reader.readAsDataURL(audioBlob);
//    reader.onloadend = function() {
//    	base64data = reader.result;
////    	console.log(base64data);
////    	console.log('size=' + base64data);
//
//    	webSocket.send(base64data);
//    }

//    var msg = bin2string(pcm);
//    //console.log( "pcm data:" + msg);
    //var array = new Uint16Array(_buf.length);
//    for(var i=0; i<pcm.length;i++) {
//    	array[i] = pcm[i];
//    }
//    var json = {
//        'title' : 'abcde',
//        'pcm' : audioBlob
//    };
    //webSocket.send(view);
    //webSocket.send(convertoFloat32ToInt16(pcm));
//    webSocket.send("111");
    //webSocket.send("aaaaaaaaaaaaaaaaaa");
    //textMessage객체의 값 초기화
    //message.value = "";
}

var convertoFloat32ToInt16 = function(buffer) {
    var l = buffer.length;
    var buf = new Int16Array(l);

    while (l--) {
        buf[l] = buffer[l]*0xFFFF;    //convert to 16 bit
    }
    return buf.buffer
};

function bin2string(array){
    var result = "";
    for(var i = 0; i < array.length; ++i){
        result+= (String.fromCharCode(array[i]));
    }
    return result;
}

function disconnect(){
    webSocket.close();
}

this.onmessage = function(e){
    switch(e.data.command){
        case 'init':
            init(e.data.config);
            break;
        case 'openWS':
            console.log("OpenWS");
            openWS(e.data.config);
            break;
        case 'closeWS':
            console.log("CloseWS");
            closeWS(e.data.config);
            break;
        case 'record':
            console.log("Record");
            record(e.data.buffer);
            break;
        case 'exportWAV':
            console.log("exportWAV");
            exportWAV(e.data.type);
            break;
        case 'exportMonoWAV':
            console.log("exportWAV");
            exportMonoWAV(e.data.type);
            break;
        case 'getBuffers':
            console.log("getBuffers");
            getBuffers();
            break;
        case 'clear':
            clear();
            break;
    }
};

function openWS(e) {
    //webSocket = new WebSocket("wss://125.132.250.243:28080/stt-demo/websocket/stt?lang="+e.lang);
    console.log( "e.type:"+ e.type + " / lang = " + e.lang);
    var v = 1;
    //var biz="biz partner";
    var biz= e.biz;
    var channel="contact channel";
    var language= e.lang;
    var service= e.service;
    var userId= e.userId;


    var param="";
    param+="v="+v;
    param+="&biz="+biz;
    param+="&channel="+channel;
    param+="&language="+language;
    param+="&service="+service;
    param+="&userId="+userId;
    if(e.type == "E" || e.type == "U") {
        param +="&answerText="+e.answerText;
    }

    var url = e.wssUrl;
    if (e.type =="S") {
        url += "/simpleStt?";
        webSocket = new WebSocket(url+param);
        //webSocket = new WebSocket("wss://125.132.250.243:28080/uapi/stt/websocket/simpleStt?v="+v+"&biz="+biz+"&channel="+channel+"&language="+e.lang);
    } else if (e.type =="E") {
        url += "/evaluationStt?";
        webSocket = new WebSocket(url+param);
        //webSocket = new WebSocket("wss://125.132.250.243:28080/uapi/stt/websocket/evaluationStt?v="+v+"&biz="+biz+"&channel="+channel+"&language="+e.lang);
    } else if (e.type =="D" ) {
        url += "/dialogStt?";

        param+="&lectureId="+e.bookId;
        param+="&chapterId="+e.chapterId;
        param+="&contentId="+e.questionId;
        webSocket = new WebSocket(url+param);
        //webSocket = new WebSocket("wss://125.132.250.243:28080/uapi/stt/websocket/dialogStt?v="+v+"&biz="+biz+"&channel="+channel+"&language="+e.lang);

    } else if (e.type =="F") {
        url += "/engFreeTalkStt?";
        param+="&biz=mindslab";
        param+="&lectureId=ml1";
        param+="&chapterId="+e.chapterId;
        param+="&contentId="+e.questionId;
        console.log("Param =", param);
        webSocket = new WebSocket(url+param);
    } else if (e.type =="U") {
        url += "/korReadingStt?";
        webSocket = new WebSocket(url+param);

    } else if (e.type =="T") {
        url += "/tutorialStt?";
        webSocket = new WebSocket(url+param);

    } else if (e.type =="SF") {
        url += "/engFreeTalkStt?";

        param+="&lectureId=B05";
        param+="&chapterId="+e.chapterId;
        param+="&contentId="+e.questionId;

        webSocket = new WebSocket(url+param);
    }

    webSocket.binaryType = 'arraybuffer';

    webSocket.onopen = function(message){
        isConnected = true;
        console.log( "Server connected\n" );
    };

    webSocket.onclose = function(message){
        console.log( "Server Disconnected\n" );
    };

    webSocket.onerror = function(message){
        console.log( "error...\n" );
        isConnected = false;
        closeWS();
    };

    webSocket.onmessage = function(message){
        sendParamMsg(message, url, param, e.type);
        sendMsg(message, e.type);
        //console.log( "Receive From Server => "+message.data+"\n" );

    };
    console.log('connect ws');
}

function sendParamMsg(msg, url, urlParam, type) {

    if(type != "SF"){
        var jsonObj = JSON.parse(msg.data);

        var param = "url="+url.split('?')[0];
        param += "&"+urlParam;
        if(type != "D") {
            param += "&userText=" + jsonObj.result.userText;
        }
        param += "&type=" + type;
        param = '{"' + param.replace(/&/g, '","').replace(/=/g, '":"') + '"}';

        //console.log(param);

        postMessage({
            command: 'request',
            message: param,
            type:   type
        });
    }

}

function sendMsg(msg, type) {

    try {
        //console.log("sendMsg:"+msg.data);
        //console.log("type:"+type);
        // var jsonObj = JSON.parse(msg.data);

        postMessage({
            command: 'response',
            message: msg.data,
            type: type
        });
    }
    catch(e){

    }
    finally {
        isConnected = false;
        closeWS();
    }
	//console.log( "sendMsg" );
}

function closeWS() {
    console.log("disconnect websocket");
    webSocket.close();
}

function init(config){
    sampleRate = config.sampleRate;
}

function record(inputBuffer){
//  console.log('record', inputBuffer[0].length);
    recBuffersL.push(inputBuffer[0]);
    recBuffersR.push(inputBuffer[1]);
    recLength += inputBuffer[0].length;
    sendMessage(inputBuffer[0]);
}

function exportWAV(type){
//  console.log('exportWav');
    var bufferL = mergeBuffers(recBuffersL, recLength);
    var bufferR = mergeBuffers(recBuffersR, recLength);
    var interleaved = interleave(bufferL, bufferR);
    var dataview = encodeWAV(interleaved);
    var audioBlob = new Blob([dataview], { type: type });

    //this.postMessage(audioBlob);
}

function exportMonoWAV(type){
//  console.log('exportMonoWav');
    var bufferL = mergeBuffers(recBuffersL, recLength);

    var startTime = new Date().getTime();
    var _buf = downsampleBuffer(bufferL, 8000);
    var endTime = new Date().getTime();
    console.log(endTime , startTime , endTime - startTime);

    var dataview = encodeWAV8k(_buf, true);
    var audioBlob = new Blob([dataview], { type: type });

    this.postMessage(audioBlob);
}

function getBuffers() {
    var buffers = [];
    buffers.push( mergeBuffers(recBuffersL, recLength) );
    buffers.push( mergeBuffers(recBuffersR, recLength) );
    this.postMessage(buffers);
}

function clear(){
    recLength = 0;
    recBuffersL = [];
    recBuffersR = [];
}

function mergeBuffers(recBuffers, recLength){
    var result = new Float32Array(recLength);
    var offset = 0;
    for (var i = 0; i < recBuffers.length; i++){
        result.set(recBuffers[i], offset);
        offset += recBuffers[i].length;
    }
    return result;
}

function interleave(inputL, inputR){
    var length = inputL.length + inputR.length;
    var result = new Float32Array(length);

    var index = 0,
        inputIndex = 0;

    while (index < length){
        result[index++] = inputL[inputIndex];
        result[index++] = inputR[inputIndex];
        inputIndex++;
    }
    return result;
}

function floatTo16BitPCM(output, offset, input){
    for (var i = 0; i < input.length; i++, offset+=2){
        var s = Math.max(-1, Math.min(1, input[i]));
        output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);
    }
}

function writeString(view, offset, string){
    for (var i = 0; i < string.length; i++){
        view.setUint8(offset + i, string.charCodeAt(i));
    }
}



function encodeWAV8k(samples, mono){
    var buffer = new ArrayBuffer(44 + samples.length * 2);
    var view = new DataView(buffer);
    console.log('encodeWAV8k');
    /* RIFF identifier */
    writeString(view, 0, 'RIFF');
    /* file length */
    view.setUint32(4, 32 + samples.length * 2, true);
    /* RIFF type */
    writeString(view, 8, 'WAVE');
    /* format chunk identifier */
    writeString(view, 12, 'fmt ');
    /* format chunk length */
    view.setUint32(16, 16, true);
    /* sample format (raw) */
    view.setUint16(20, 1, true);
    /* channel count */
    view.setUint16(22, mono?1:2, true);
    /* sample rate */
    view.setUint32(24, 8000, true);
    /* byte rate (sample rate * block align) */
    view.setUint32(28, 8000 * 4, true);
    /* block align (channel count * bytes per sample) */
    view.setUint16(32, 4, true);
    /* bits per sample */
    view.setUint16(34, 16, true);
    /* data chunk identifier */
    writeString(view, 36, 'data');
    /* data chunk length */
    view.setUint32(40, samples.length * 2, true);

    floatTo16BitPCM(view, 44, samples);

    return view;
}

function encodeWAV(samples, mono){
    var buffer = new ArrayBuffer(44 + samples.length * 2);
    var view = new DataView(buffer);

    /* RIFF identifier */
    writeString(view, 0, 'RIFF');
    /* file length */
    view.setUint32(4, 32 + samples.length * 2, true);
    /* RIFF type */
    writeString(view, 8, 'WAVE');
    /* format chunk identifier */
    writeString(view, 12, 'fmt ');
    /* format chunk length */
    view.setUint32(16, 16, true);
    /* sample format (raw) */
    view.setUint16(20, 1, true);
    /* channel count */
    view.setUint16(22, mono?1:2, true);
    /* sample rate */
    view.setUint32(24, sampleRate, true);
    /* byte rate (sample rate * block align) */
    view.setUint32(28, sampleRate * 4, true);
    /* block align (channel count * bytes per sample) */
    view.setUint16(32, 4, true);
    /* bits per sample */
    view.setUint16(34, 16, true);
    /* data chunk identifier */
    writeString(view, 36, 'data');
    /* data chunk length */
    view.setUint32(40, samples.length * 2, true);

    floatTo16BitPCM(view, 44, samples);

    return view;
}

function downsampleBuffer(buffer, rate) {
    if (rate == sampleRate) {
        return buffer;
    }
    if (rate > sampleRate) {
        throw "downsampling rate show be smaller than original sample rate";
    }
    var sampleRateRatio = sampleRate / rate;
    var newLength = Math.round(buffer.length / sampleRateRatio);
    var result = new Float32Array(newLength);
    var offsetResult = 0;
    var offsetBuffer = 0;
    while (offsetResult < result.length) {
        var nextOffsetBuffer = Math.round((offsetResult + 1) * sampleRateRatio);
        var accum = 0, count = 0;
        for (var i = offsetBuffer; i < nextOffsetBuffer && i < buffer.length; i++) {
            accum += buffer[i];
            count++;
        }
        result[offsetResult] = accum / count;
        offsetResult++;
        offsetBuffer = nextOffsetBuffer;
    }
    return result;
}
