// MINDsLab. UX/UI Team. YMJ. 20180703

$(document).ready(function (){	
	// chatbot open
	$('#btn_flt_cb, #btn_chat_open').on('click', function() {
		var winWidth = $(window).width();	
		var winHeight = $(window).height();	
		if ( winWidth < 760) {
			$('#cahtbotWrap').animate({
				'height': '100%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': 'hidden',	
			});	
			$('.chatbot_contents .chat_mid').css({
				'height':winHeight-130,	
			});	
				
		} else {
			$('#cahtbotWrap').animate({
				'height': '80%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': '',	
			});
		}
		//$('#btn_bot_aitrago').trigger('click');
		$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);	
		
		$('.objectBox .phone').animate({
			'right':'-150px',
			'opacity':0,
		},300);
		
	});
	
	// cahtbot nav active
	$('.cahtbot_nav ul li button').on('click', function(){
		$('.cahtbot_nav ul li button').removeClass('active'); 
		$(this).addClass('active'); 
	});
	
	// Layer popup close
	$('#btn_layerClose').on('click', function(){
		$('.layerPop').fadeOut(); 
	});
	
	// Layer popup open
	$('#btn_apply_open').on('click', function(){
		$('.layerPop').fadeIn(); 
		$('.applyBox .ipt_txt').each(function(){					
			$(this).val('');
		});
	});
		
});

$(window).resize(function (){		
	// chatbot open
	$('#btn_flt_cb, #btn_chat_open').on('click', function() {
		var winWidth = $(window).width();
		if ( winWidth < 760) {
			$('#cahtbotWrap').animate({
				'height': '100%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': 'hidden',	
			});		
		} else {
			$('#cahtbotWrap').animate({
				'height': '80%',	
				'opacity': '1.0',	
			},300);
			
			$('#maumWrap').css({
				'overflow': '',	
			});	
		}
		//$('#btn_bot_aitrago').trigger('click');
		$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);	
		
		
	});
});

// scroll event
$(window).scroll(function(){
	var st = $(window).scrollTop();
	
	if(st > 1) {	
		// wide image animation transform: matrix(1.08584, 0, 0, 1.08584, 0, -33.5788);
		$('.crop_cell .bg_aniBox img').css({transform: 'matrix(1.08584, 0, 0, 1.08584, 0,' + st * 0.0754});	
	} else {	
		// wide image animation
		$('.crop_cell .bg_aniBox img').css({transform: 'matrix(1.08584, 0, 0, 1.08584, 0,' - st * 0.0754});	
	}	

	$('#object01').each(function(){
		var winWidth = $(window).width();
		var objectOffset = $('#object01').offset().top;
		
		if ( winWidth < 760) {
			
			
			if(st > objectOffset - 1550) {
				$('#object01').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object01').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
			
			if(st > objectOffset - 1200) {
				$('#object02').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object02').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
			
			if(st > objectOffset - 850) {
				$('#object03').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object03').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}	
		} else {
			if(st > objectOffset - 800) {
				$('#object01').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object01').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
			
			if(st > objectOffset - 700) {
				$('#object02').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object02').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
			
			if(st > objectOffset - 620) {
				$('#object03').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object03').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
		}
	});	

	
	
	
	

	
	
});

