// MINDsLab. UX/UI Team. YMJ. 20180703

// layerpopup
(function ( $ ) { 
    $.fn.movLayer = function(options) {

        var movLayerOptions = $.extend({
                'autoplay': 1
        }, options );

        $(this).on('click', function (e) {

            var movLayerLink = $(this).attr("href");

            if( movLayerLink.match(/(youtube.com)/) ){
                var split_c = "v=";
                var split_n = 1;
            }

            if( movLayerLink.match(/(youtu.be)/) || movLayerLink.match(/(vimeo.com\/)+[0-9]/) ){
                var split_c = "/";
                var split_n = 3;
            }

            if( movLayerLink.match(/(vimeo.com\/)+[a-zA-Z]/) ){
                var split_c = "/";
                var split_n = 5;
            }

            var getYouTubeVideoID = movLayerLink.split(split_c)[split_n];

            var cleanVideoID = getYouTubeVideoID.replace(/(&)+(.*)/, "");

            if( movLayerLink.match(/(youtu.be)/) || movLayerLink.match(/(youtube.com)/) ){
                var videoEmbedLink = "https://www.youtube.com/embed/"+cleanVideoID+"?autoplay="+movLayerOptions.autoplay+"";
            }

            if( movLayerLink.match(/(vimeo.com\/)+[0-9]/) || movLayerLink.match(/(vimeo.com\/)+[a-zA-Z]/) ){
                var videoEmbedLink = "https://player.vimeo.com/video/"+cleanVideoID+"?autoplay="+movLayerOptions.autoplay+"";
            }

            $("body").append('<div class="layerpopup_wrap"><div class="layer_bg"></div><div class="layerBox"><a href="#none" class="layer_close"></a><iframe src="'+videoEmbedLink+'" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe></div></div>');
			
            $(".layer_bg, .layer_close").click(function(){
                $(".layerpopup_wrap").addClass("layerpopup_hide").delay(500).queue(function() { $(this).remove(); });
            });
						
			var winWidth = $(window).width();	
			var winHeight = $(window).height();
			var layerWidth = $('.lot_c').width();			
			var layerHeight = (9 / 16) * layerWidth;

			// =*= Pc Layout =*=	
			$('.layerpopup_wrap .layerBox').css({
				'width' : layerWidth,
				'height' : layerHeight,	
				'margin-top' : -layerHeight/2,	
				'margin-left' : -layerWidth/2,	
			});								

            e.preventDefault();
        });
    }; 
}( jQuery ));
$(document).ready(function (){	
	//aside menu
	var clicked = false;	

	$('a.btn_header_ham').click(function(){
		if (!clicked) {
			$(this).addClass('active');
			$('.aside').css({
				width : 'calc(100% - 50px)',
			},{duration:200,queue:false});
			$('.aside').show();	
			$('.btn_goTop').hide();
			
			$('.bg_aside').animate({
				  opacity : 0.7,
			},{duration:200,queue:false});
			$('.bg_aside').css({
				  display : 'block',
			});

			clicked=true;
		} else {
			$(this).removeClass('active');
			$('.aside').animate({
				width: '0',
			},{duration:200,queue:false});
			$('.aside').hide();	
			$('.btn_goTop').show();

			$('.bg_aside').animate({
				  opacity : '0',
			},{duration:150,queue:false});
			$('.bg_aside').css({
				  display : 'none',
			});

			clicked=false;
		}
	});
	
	
	$('.bg_aside').on('click',function(){
		$('a.btn_header_ham').removeClass('active');
		$('.aside').animate({
			width: '0',
		},{duration:200,queue:false});
		$('.btn_goTop').show();

		$('.bg_aside').animate({
			  opacity : 0,
		},{duration:150,queue:false});
		$('.bg_aside').css({
			  display : 'none',
		});

		clicked=false;
	});

	// Hide Header on on scroll down 
	var didScroll; 
	var lastScrollTop = 0; 
	var delta = 5; 
	var navbarHeight = $('#header').outerHeight(); 
	
	$(window).scroll(function(event){ 
		didScroll = true; 
	}); 
	setInterval(function() { 
		if (didScroll) { 
			hasScrolled(); 
			didScroll = false; 
		} 
	}, 250); 
	function hasScrolled() { 
		var scrollTop = $(window).scrollTop();
		
		// Make sure they scroll more than delta 
		if(Math.abs(lastScrollTop - scrollTop) <= delta) 
			return; 
		
		// If they scrolled down and are past the navbar, add class .nav-up. 
		// This is necessary so you never see what is "behind" the navbar. 
		
		if (scrollTop > 10){ 
			// Scroll Down 
			$('#maumWrap').addClass('fixed'); 
			$('.bnrWrap').css({
				height: '0',
			}); 
			// top button
			$('.btn_goTop').fadeIn();
		} else { 
			// Scroll Up 
			if(scrollTop + $(window).height() < $(document).height()) { 
				$('#maumWrap').removeClass('fixed'); 
				$('.bnrWrap').css({
					height: '',
				}); 
				// top button
				$('.btn_goTop').fadeOut();
			}
		} 
		lastScrollTop = scrollTop; 
	}
	
	// language (pc)
	$('#header .sta .etcMenu ul li.lang').each(function(){
		$(this).on('click',function(){
			$(this).toggleClass('active'); 
		});	
		$('#header .sta .etcMenu ul li.lang ul.lst li a').on('click',function(){
			$(this).removeClass('active'); 
		});	
		$('#container').on('click',function(){
			$('#header .sta .etcMenu ul li.lang').removeClass('active'); 
		});	
	});	
	
	// language (mobile)
	$('ul.m_etcMenu li.lang').each(function(){
		$(this).on('click',function(){
			$(this).toggleClass('active'); 
		});	
		$('ul.m_etcMenu li.lang ul.lst li a').on('click',function(){
			$(this).removeClass('active'); 
		});	
	});	
	
	$('.tab_mov').each(function(){
		$('.tab_mov .tab_nav li:first-child a').addClass('active').show(); //Activate first tab	
		//TAB On Click Event
		$('.tab_mov .tab_nav li a').on('click', function(){	
			$('.tab_mov .tab_nav li a').removeClass('active'); //Remove any 'active' class
			$(this).addClass('active'); //Add 'active' class to selected tab				
		});		
		 $(".tab_mov .tab_nav li a").click(function(event){            
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top-100}, 400);
        });
		
	});	


	var tbOffset = $( '.tab_nav' ).offset();
    $( window ).scroll( function() {
      if($('.tab_nav').is(":visible")){
        if ( $( document ).scrollTop() > tbOffset.top-50 ) {
            $( '.tab_nav' ).addClass( 'tbFixed' );
        }
        else {
            $( '.tab_nav' ).removeClass( 'tbFixed' );
        }
      }
    });
	
	// section move
	$('.gnb ul.gnbNav li h2 a[href^="#"]').on('click', function (e) {
		e.preventDefault();

		var target = this.hash;
		var $target = $(target);

		$('html, body').animate({
			'scrollTop': $target.offset().top
		}, 700, 'swing');
		$('.gnb ul.gnbNav li h2 a').removeClass('active');
		$(this).addClass('active');
	});
		
	// Mobile contact us button
	$('#m_api_contac').on('click',function(){
		$('a.btn_header_ham').removeClass('active');
		$('.aside').animate({
			width: '0',
		},{duration:200,queue:false});
		$('.btn_goTop').show();

		$('.bg_aside').animate({
			  opacity : 0,
		},{duration:150,queue:false});
		$('.bg_aside').css({
			  display : 'none',
		});

		clicked=false
		
		e.preventDefault();

		var target = this.hash;
		var $target = $(target);

		$('html, body').animate({
			'scrollTop': $target.offset().top
		}, 700, 'swing');
	});	
		
	$('#btn_checkup').on('click',function(){
		$('.checkupBox .info').toggle();
	});	
	
	
	// chatbot open
	$('#btn_flt_cb').on('click', function() {
		var winWidth = $(window).width();	
		var winHeight = $(window).height();	
		if ( winWidth < 760) {
			$('#livechatWrap').fadeIn(300);	
			$('#wrap').css({
				'overflow': 'hidden',	
			});	
			$('#livechatWrap .chatbot_contents .chat_mid').css({
				'height':winHeight-130,	
			});	
			$('#livechatWrap .chatbot_contents .talkLst').css({
				'display':'block',
			});
				
		} else {
			$('#livechatWrap').fadeIn(300);		
			$('#maumWrap').css({
				'overflow': '',	
			});	
			$('#livechatWrap .chatbot_contents .talkLst').css({
				'display':'block',
			});
		}
	});
		
	// top button
	$('.btn_goTop').on('click',function(){
		$('html, body').animate({scrollTop: 0}, 500);
	});	
		
	// cahtbot nav active
	$('.cahtbot_nav ul li button').on('click', function(){
		$('.cahtbot_nav ul li button').removeClass('active'); 
		$(this).addClass('active'); 
	});
	
	// Layer popup close
	$('#btn_layerClose').on('click', function(){
		$('.layerPop').fadeOut(); 
	});
	
	// Layer popup open
	$('#btn_apply_open').on('click', function(){
		$('.layerPop').fadeIn(); 
		$('.applyBox .ipt_txt').each(function(){					
			$(this).val('');
		});
	});
	
	// apply
	$('#btn_apply').on('click', function(){
		$('.layerPop').fadeOut(); 
		alert('신청되었습니다.')
	});	
	
	$('.bg_aniBox').each(function(){
		var aniboxSize = $(this).parent().height();
		$(this).css({
			height: aniboxSize,
		}); //Hide all content
	});	
	
	// view top mov icon
	// $('a.btn_movLayer').each(function(){
	// 	$(this).children().children().after('<strong class="ico_mov">동영상</strong>');
	// });
	
	// banner
	$('.bnrWrap .btn_bnr_close').on('click', function () {
		$('.bnrWrap').animate({
			height:'0',
		},100);
		$('#maumWrap').addClass('normal');
	});
});

$(window).resize(function (){		
	$('.productWrap .chatbot_contents .chat_mid').each(function(){
		var winWidth = $(window).width();
		var chatMidHeight = $(window).height()-350;
		if ( winWidth < 760) {
			$('.productWrap .chatbot_contents .chat_mid').css({
				'height': chatMidHeight,	
			});	
			$('.productWrap .product_bd .product_cont').css({
				'height': chatMidHeight+130,	
			});	
		}  else {
			$('.productWrap .chatbot_contents .chat_mid').css({
				'height': 'calc(100% - 65px)',	
			});
			$('.productWrap .product_bd .product_cont').css({
				'height': '',	
			});
		}
	});	
});

// scroll event
$(window).scroll(function(){
	var st = $(window).scrollTop();
	
	if(st > 1) {	
		// wide image animation transform: matrix(1.08584, 0, 0, 1.08584, 0, -33.5788);
		$('.crop_cell .bg_aniBox img').css({transform: 'matrix(1.08584, 0, 0, 1.08584, 0,' + st * 0.0754});	
	} else {	
		// wide image animation
		$('.crop_cell .bg_aniBox img').css({transform: 'matrix(1.08584, 0, 0, 1.08584, 0,' - st * 0.0754});	
	}	

	$('#object01').each(function(){
		var winWidth = $(window).width();
		var objectOffset = $('#object01').offset().top;
		
		if ( winWidth < 760) {
			
			
			if(st > objectOffset - 1550) {
				$('#object01').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object01').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
			
			if(st > objectOffset - 1200) {
				$('#object02').animate({
					'left':'0',
					'opacity':1,
				},400);
			} else {
				$('#object02').animate({
					'left':'0',
					'opacity':0,
				},500);
			}
			
			if(st > objectOffset - 850) {
				$('#object04').animate({
					'right':'0',
					'opacity':1,
				},500);
			} else {
				$('#object04').animate({
					'right':'0',
					'opacity':0,
				},300);
			}	
			if(st > objectOffset - 750) {
				$('#object05').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object05').animate({
					'top':'0',
					'opacity':0,
				},300);
			}
			if(st > objectOffset - 450) {
				$('#object03').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object03').animate({
					'top':'0',
					'opacity':0,
				},300);
			}
		} else {
			if(st > objectOffset - 800) {
				$('#object01').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object01').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
			
			if(st > objectOffset - 700) {
				$('#object02').animate({
					'left':'0',
					'opacity':1,
				},400);
			} else {
				$('#object02').animate({
					'left':'0',
					'opacity':0,
				},400);
			}
			
			if(st > objectOffset - 620) {
				$('#object04').animate({
					'right':'0',
					'opacity':1,
				},500);
			} else {
				$('#object04').animate({
					'right':'0',
					'opacity':0,
				},500);
			}
			if(st > objectOffset - 520) {
				$('#object05').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object05').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
			if(st > objectOffset - 420) {
				$('#object03').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object03').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
		}
	});		
});

