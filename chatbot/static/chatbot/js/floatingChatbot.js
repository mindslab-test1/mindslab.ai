// MINDsLab. UX/UI Team. YMJ. 20180613

//chatting info setting
var CHATBOT = '';
var deviceId = "";
var pre_intervened_state = false;
var cur_intervened_state = false;
var img_sully = "/static/chatbot/images/mybot.png";
var img_counselor = "/static/chatbot/images/img_ttsperson.png";

$(document).ready(function (){
	// 날짜, 요일 시간 정의	
	var year  = new Date().getFullYear();  //현재 년도
	var month = new Date().getMonth()+1;  //현재 월
	var date  = new Date().getDate();  //현재 일
	var week  = new Array('일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일');	  //요일 정의
	var thisWeek  = week[new Date().getDay()];	//현재 요일

	// 오늘 날짜 입력
	$('.talkLst li.newDate span').each(function(){
		$(this).append(year + '년 ' + month +'월 ' + date +'일 ' + thisWeek);
	});	

	// 첫멘트 시간 표시
    $('.chatbot_contents .chat_mid .talkLst li.bot .cont:last-child').append('<em class="date"><b>' + getAmPm() + '</b>' + getTime() + '</em>');
	// 내용있을 시 스크롤 최하단
	$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);
	
	// 채팅입력 (Shift + Enter)
	$('.chatbot_contents .chat_btm .textArea').keyup(function (event) {
		if (event.keyCode == 13 && event.shiftKey) {
			var chatTxt = this.value;
			var caret = getCaret(this);
			this.value = chatTxt.substring(0,caret)+"\n"+chatTxt.substring(carent,chatTxt.length-1);
			event.stopPropagation();
		} else if (event.keyCode == 13){
			$('#btn_chat').trigger('click');
		}
	});

	

	// 채팅입력 (text 출력)
	$('#btn_chat').on('click', function() {
        
		// textarea 텍스트 값 및 엔터처리
        var textValue = $('.chatbot_contents .chat_btm .textArea').val().trim().replace(/(?:\r\n|\r|\n)/g, '<br>');
        var img_src = "";
        cur_intervened_state ? img_src=img_counselor : img_src=img_sully; //이미지 세팅
		if( $('.chatbot_contents .chat_btm .textArea').val().replace(/\s/g,"").length == 0){ 
            // text가 없으면 실행 
		} else {
            document.getElementById("btn_chat").disabled = true;
            document.getElementById("textline").disabled = true;
            document.getElementById("textline").setAttribute("placeholder","답변을 기다리는 중입니다.");

            if($('.talkLst .bot:last-child .cont .txt span').hasClass('chatLoading')){
                $('.talkLst .bot:last-child').remove();
            }

            $('.chatbot_contents .chat_mid .talkLst').append(
				'<li class="user"> \
					<span class="cont"> \
                        <em class="txt">' + textValue + '</em> \
                        <em class="date"><b>' + getAmPm() + '</b>' + getTime() + '</em> \
					</span> \
				</li>'
            );
            
            $('.chatbot_contents .chat_mid .talkLst').append(
                '<li class="bot">\
                    <span class="thumb"><img src="'+ img_src + '" alt="sully"></span>\
                    <span class="cont">\
                        <!-- 로딩 -->\
                        <em class="txt">\
                            <span class="chatLoading">\
                                <b class="chatLoading_item01"></b>\
                                <b class="chatLoading_item02"></b>\
                                <b class="chatLoading_item03"></b>\
                            </span>\
                        </em>\
                    </span>\
                </li>'
            );
            
            sendTalk(textValue);

			$('.chatbot_contents .bot_infoBox').css({
				'display':'none',
			});
			$('.chatbot_contents .talkLst').css({
				'display':'block',
			});
			$('.chatbot_contents .chat_btm .textArea').val('');
			var winWidth = $(window).width();
			if ( winWidth < 760) {
				$('#cahtbotWrap').each(function(){
					var cahtbotWrapHeight = $('#cahtbotWrap').height();
					$('.chatbot_contents .chat_mid').css({
						'height': Math.round(cahtbotWrapHeight-130),
					});
				});
			} else {
				$('#cahtbotWrap').each(function(){
					var cahtbotWrapHeight = $('#cahtbotWrap').height();
					$('.chatbot_contents .chat_mid').css({
						'height': Math.round(cahtbotWrapHeight-145),
					});
				});
			}
			$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);
		}
	});
});


// 추천질문 (text 출력)
$('.info_btnBox li button').on('click', function() {
    var recomQust = $(this).text();

    $('.chatbot_contents .chat_btm .textArea').val(recomQust);
    $('#btn_chat').trigger('click');

    $('.chatbot_contents .bot_infoBox').css({
        'display':'none',
    });
    $('.chatbot_contents .talkLst').css({
        'display':'block',
    });

    var winWidth = $(window).width();
    if ( winWidth < 760) {
    $('#cahtbotWrap').each(function(){
        var cahtbotWrapHeight = $('#cahtbotWrap').height();
        $('.chatbot_contents .chat_mid').css({
            'height': Math.round(cahtbotWrapHeight-130),
        });
    });
    } else {
        $('#cahtbotWrap').each(function(){
            var cahtbotWrapHeight = $('#cahtbotWrap').height();
            $('.chatbot_contents .chat_mid').css({
                'height': Math.round(cahtbotWrapHeight-145),
            });
        });
    }
    $('.chatbot_contents .chat_btm .textArea').val('');
    $('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);
});


function getAmPm(){
    return new Date().getHours() >= 12 ? '오후' : '오전';
}
function getTime(){
    var	thisHours = new Date().getHours() >=13 ?  new Date().getHours()-12 : new Date().getHours(); //현재 시
	var	thisMinutes = new Date().getMinutes() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes(); //현재 분
    return thisHours + ':' + thisMinutes;
}

function  connectionEndMsg(){
    $('.chatbot_contents .chat_mid .talkLst').append(
        '<li class="commentTxt"> \
            <span>담당자와의 연결이 종료되었습니다. </span> \
        </li>\
        <li class="bot">\
            <span class="thumb"><img id="thumb" src='+img_sully+ ' alt="sully"></span>\
            <span class="cont">\
                <em class="txt">\
                    궁금하신 내용을 말씀해주시겠어요?\
                    <br><br>\
                    조금 더 상세한 문의를 원하신다면 아래의 "담당자 연결" 버튼을 통해 전문 담당자와 상담이 가능합니다.\
                </em>\
            </span>\
            <ul class="info_btnBox">\
                <li><button type="button" onclick="btnTextToTalk($(this).text())">회사가 어디에 있나요?</button></li>\
                <li><button type="button" onclick="btnTextToTalk($(this).text())">채용 일정 알려주세요</button></li>\
                <li><button type="button" onclick="btnTextToTalk($(this).text())">회사 소개서 보여주세요</button></li>\
                <li><button type="button" onclick="btnTextToTalk($(this).text())">담당자 연결</button></li>\
            </ul>\
            <em class="date"><b>' + getAmPm() + '</b>' + getTime() + '</em> \
        </li>'
    );
    document.getElementById("btn_chat").disabled = false;
    document.getElementById("textline").disabled = false;
    document.getElementById("textline").setAttribute("placeholder","메세지를 입력해 주세요.");
}


function btnTextToTalk(recomQust) {
	$('.chatbot_contents .chat_btm .textArea').val(recomQust);
	$('#btn_chat').trigger('click');

	$('.chatbot_contents .bot_infoBox').css({
		'display':'none',
	});
	$('.chatbot_contents .talkLst').css({
		'display':'block',
	});

	$('.chatbot_contents .chat_btm .textArea').val('');
	$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);
}

function sendTalk(talkMessage) {
    talkMessage = talkMessage.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, " ");
	// console.log("talk deviceId: "+deviceId);
    // console.log('session id : ', document.getElementById('SESSION_ID').value);
    
    if(talkMessage =="담당자 연결"){
        setTimeout(function(){
            $('.talkLst .bot:last-child').remove(); //loading요소 지우기
            $('.chatbot_contents .chat_mid .talkLst').append(
                '<li class="bot"> \
                    <span class="thumb"><img src="' + img_sully + '"></span>\
                    <span class="cont"> \
                    <em class="txt">전문 담당자와의 연결을 위해 봇을 종료합니다.</em> \
                    <em class="date"><b>' + getAmPm() + '</b>' + getTime() + '</em> \
                    </span> \
                </li>'
            );
            $('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);
        },200);
    }

	$.ajax({
		url: 'https://aicc-prd1.maum.ai:9980/api/v3/dialog/textToTextTalk',
		async: true,
		type: 'POST',
		headers: {
			"Content-Type": "application/json",
			"m2u-auth-internal": "m2u-auth-internal"
			//"Authorization": document.getElementById('AUTH_ID').value
		},
		data: JSON.stringify({
              "payload": {"utter": talkMessage, "lang": "ko_KR"},
              "device": {
                "id": deviceId,
                "type": "WEB",
                "version": "0.1",
                "channel": "ADMINWEB"
              },
              "location": {"latitude": 10.3, "longitude": 20.5, "location": "mindslab"},
              "authToken": document.getElementById('AUTH_ID').value
         }),
		dataType: 'json',
		success: function(data) {
            // console.log(data);
            var img_src = img_sully;
            var prevent_bot = false;

            if(data.directive === undefined){//exceptioin
                if($('.talkLst .bot:last-child .cont .txt span').hasClass('chatLoading')){//loading요소 지우기
                    $('.talkLst .bot:last-child').remove();
                }
                connectionEndMsg();
                cur_intervened_state = false;
                pre_intervened_state = false;
                document.getElementById("btn_chat").disabled = false;
                document.getElementById("textline").disabled = false;
                document.getElementById("textline").setAttribute("placeholder","메세지를 입력해 주세요.");
                $('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight); // 내용있을 시 스크롤 최하단

                return;
            }

            cur_intervened_state = data.directive.payload.response.meta["m2u.intervenedAnswer"]; //상담원 연결시 true

            if (cur_intervened_state === undefined) {//m2u.intervenedAnswer 값이 없는 경우
                cur_intervened_state = false;
            }

            if (pre_intervened_state != cur_intervened_state) { // ["m2u.intervenedAnswer"]상태 변경시
                if($('.talkLst .bot:last-child .cont .txt span').hasClass('chatLoading')){ //loading UI 지우기
                    $('.talkLst .bot:last-child').remove();
                }
                if (cur_intervened_state) {
                    $('.chatbot_contents .chat_mid .talkLst').append(
                        '<li class="commentTxt"> \
                            <span>담당자와 연결되었습니다. </span> \
                        </li>'
                    );
                } else {
                    prevent_bot = true;
                    connectionEndMsg();
                }
            }

            // 채팅 이미지 세팅
            if(cur_intervened_state){ img_src = img_counselor; } 
            else{ img_src = img_sully; }
            pre_intervened_state = cur_intervened_state; 

            $('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight); // 내용있을 시 스크롤 최하단
            // let answer_obj = make_utter_obj(data.answer);
            if(!prevent_bot){ // 상담원 연결 종료 후, greeting msg를 이미 넣어줌. 새로운 msg 나오지 않도록.
                $.ajax({
                    url: '/process_utter',
                    async: true,
                    type: 'GET',
                    // headers: {
                    //     "Content-Type": "application/json",
                    // },
                    data: {
                        "utter_text": data.directive.payload.response.speech.utter,
                    },
                    dataType: 'json',
                    success: function(result) {
                        // console.log(result);
                        if($('.talkLst .bot:last-child .cont .txt span').hasClass('chatLoading')){ //loading UI 지우기
                            $('.talkLst .bot:last-child').remove();
                        }
                        $('.chatbot_contents .chat_mid .talkLst').append(
                            '<li class="bot"> \
                                <span class="thumb"><img src="' + img_src + '"></span>\
                                <span class="cont"> \
                                    <em class="txt">' + result.utter + '</em> \
                                </span> \
                            </li>'
                        );
                        // 이미지
                        if (result.image) {
                            $('.chatbot_contents .chat_mid .talkLst .bot:last-child .cont').append(result.image);
                        }
                        // 썸네일
                        if (result.thumbnail) {
                            $('.chatbot_contents .chat_mid .talkLst .bot:last-child .cont').append(result.thumbnail);
                        }
                        // 세로버튼
                        if (result.v_btn) {
                            $('.chatbot_contents .chat_mid .talkLst .bot:last-child .cont').append(result.v_btn);
                        }
                        // 가로버튼
                        if (result.h_btn) {
                            $('.chatbot_contents .chat_mid .talkLst .bot:last-child .cont').append(result.h_btn);
                        }
    
                        $('.chatbot_contents .chat_mid .talkLst .bot:last-child .cont:last-child').append(
                            '<em class="date"><b>' + getAmPm() + '</b>' + getTime() + '</em>'
                            );
                        $('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight); // 내용있을 시 스크롤 최하단

                        document.getElementById("btn_chat").disabled = false;
                        document.getElementById("textline").disabled = false;
                        document.getElementById("textline").setAttribute("placeholder","메세지를 입력해 주세요.");

                    }, error: function(err) {
                        console.log("parse HTML error! ", err);
                    }
                });
            }
		}, error: function(err) {
			console.log("chatbot SendTalk error! ", err);
		}
	});
}

function closeDialogService() {
// console.log("close");
	$.ajax({
		url: 'https://aicc-prd1.maum.ai:9980/api/v3/dialog/close',
		async: true,
		type: 'POST',
		headers: {
			"Content-Type": "application/json",
			"m2u-auth-internal": "m2u-auth-internal"
		},
		data: JSON.stringify({
            "device": {
                "id": deviceId,
                "type": "WEB",
                "version": "0.1",
                "channel": "ADMINWEB"
            },
            "location": {"latitude": 10.3, "longitude": 20.5, "location": "mindslab"},
            "authToken": document.getElementById('AUTH_ID').value
		}),
		// dataType: 'json',
		success: function(data) {
			// console.log(data);
		}, error: function(err) {
			console.log("chatbot Close Dialog error! ", err);
		}
	});
}

function chatOpen(chatbot) {
    pre_intervened_state = false;
    cur_intervened_state = false;
    deviceId = 'MAUM_'+ this.randomString();
	$.ajax({
		url: 'https://aicc-prd1.maum.ai:9980/api/v3/dialog/open',
		async: false,
		type: 'POST',
		headers: {
			"Content-Type": "application/json",
			"m2u-auth-internal": "m2u-auth-internal"
		},
		data: JSON.stringify({
			"payload": {
			  "utter": "UTTER1",
			  "chatbot": chatbot,
			  "skill": "SKILL1"
			},
			"device": {
			  "id": deviceId ,
			  "type": "WEB",
			  "version": "0.1",
			  "channel": "ADMINWEB"
			},
			"location": {"latitude": 10.3, "longitude": 20.5, "location": "mindslab"},
			"authToken": document.getElementById('AUTH_ID').value
		}),
		dataType: 'json',
		success: function(data) {
			// console.log(data);
			// console.log("deviceId: "+deviceId);
			//document.getElementById('AUTH_ID').value = data.directive.payload.authSuccess.authToken;
			//console.log(document.getElementById('AUTH_ID').value);
			// console.log('open success!')
			// openDialogService(chatbot);
		}, error: function(err) {
			console.log("chatbot Login error! ", err);
		}
	});
}

// parse og meta tag when they exist
function make_utter_obj(data) {
    // console.log(data.utter);
    $.ajax({
        url: '/process_utter',
        async: true,
        type: 'GET',
        // headers: {
        //     "Content-Type": "application/json",
        // },
        data: {
            "utter_text": data.utter,
        },
        dataType: 'json',
        success: function(data) {
            // console.log(data);
            return data;
        }, error: function(err) {
            console.log("parse HTML error! ", err);
        }
    });
}

function  randomString() {
    const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    const string_length = 15;
    let randomstring = '';
    for (let i = 0; i < string_length; i++) {
      let rnum = Math.floor(Math.random() * chars.length);
      randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}