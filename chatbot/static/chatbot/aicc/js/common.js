// MINDsLab. UX/UI Team. YMJ. 20180703

$(document).ready(function (){
	//aside menu
	var clicked = false;	
	var asideWidth = $(window).width() - 50;
	$('.aside').show();

	$('a.btn_header_ham').click(function(){
		if (!clicked) {
			$(this).addClass('active');
			$('.aside').animate({
				width : asideWidth,
			},{duration:200,queue:false});
			$('.btn_goTop').hide();
			
			$('.bg_aside').animate({
				  opacity : 0.7,
			},{duration:200,queue:false});
			$('.bg_aside').css({
				  display : 'block',
			});

			clicked=true;
		} else {
			$(this).removeClass('active');
			$('.aside').animate({
				width: '0',
			},{duration:200,queue:false});
			$('.btn_goTop').show();

			$('.bg_aside').animate({
				  opacity : '0',
			},{duration:150,queue:false});
			$('.bg_aside').css({
				  display : 'none',
			});

			clicked=false;
		}
	});
	
	$('.bg_aside').on('click',function(){
		$('a.btn_header_ham').removeClass('active');
		$('.aside').animate({
			width: '0',
		},{duration:200,queue:false});
		$('.btn_goTop').show();

		$('.bg_aside').animate({
			  opacity : 0,
		},{duration:150,queue:false});
		$('.bg_aside').css({
			  display : 'none',
		});

		clicked=false;
	});

	// Hide Header on on scroll down 
	var didScroll; 
	var lastScrollTop = 0; 
	var delta = 5; 
	var navbarHeight = $('#header').outerHeight(); 
	
	$(window).scroll(function(event){ 
		didScroll = true; 
	}); 
	setInterval(function() { 
		if (didScroll) { 
			hasScrolled(); 
			didScroll = false; 
		} 
	}, 250); 
	function hasScrolled() { 
		var scrollTop = $(window).scrollTop();
		
		// Make sure they scroll more than delta 
		if(Math.abs(lastScrollTop - scrollTop) <= delta) 
			return; 
		
		// If they scrolled down and are past the navbar, add class .nav-up. 
		// This is necessary so you never see what is "behind" the navbar. 
		
		if (scrollTop > 10){ 
			// Scroll Down 
			$('#header').addClass('fixed'); 
			// top button
			$('.btn_goTop').fadeIn();
			$('.btn_header_ham').css({
				'position':' absolute',
			});
		} else { 
			// Scroll Up 
			if(scrollTop + $(window).height() < $(document).height()) { 
				$('#header').removeClass('fixed'); 
				// top button
				$('.btn_goTop').fadeOut();
				$('.btn_header_ham').css({
					'position':'fixed',
				});
			}
		} 
		lastScrollTop = scrollTop; 
	}
	
	// section move
	$('.gnb ul.gnbNav li h2 a[href^="#"]').on('click', function (e) {
		e.preventDefault();

		var target = this.hash;
		var $target = $(target);

		$('html, body').animate({
			'scrollTop': $target.offset().top
		}, 700, 'swing');
		$('.gnb ul.gnbNav li h2 a').removeClass('active');
		$(this).addClass('active');
	});
		
	// Mobile contact us button
	$('#m_api_contact').on('click',function(){
		$('a.btn_header_ham').removeClass('active');
		$('.aside').animate({
			width: '0',
		},{duration:200,queue:false});
		$('.btn_goTop').show();

		$('.bg_aside').animate({
			  opacity : 0,
		},{duration:150,queue:false});
		$('.bg_aside').css({
			  display : 'none',
		});

		clicked=false
		
		e.preventDefault();

		var target = this.hash;
		var $target = $(target);

		$('html, body').animate({
			'scrollTop': $target.offset().top
		}, 700, 'swing');
	});	
		
	
	// chatbot open
	$('#btn_flt_cb, #btn_chat_open').on('click', function() {
		var winWidth = $(window).width();	
		var winHeight = $(window).height();	
		if ( winWidth < 760) {
			$('#cahtbotWrap').animate({
				'height': '100%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': 'hidden',	
			});	
			$('.chatbot_contents .chat_mid').css({
				'height':winHeight-130,	
			});	
				
		} else {
			$('#cahtbotWrap').animate({
				'height': '80%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': '',	
			});	
			//$('#btn_bot_sully').trigger('click');
			$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);	
		}
	});
	
	// sully open
	$('#cahtbot_rolling li.ico_sully a').on('click', function() {
		var winWidth = $(window).width();
		if ( winWidth < 760) {
			$('#cahtbotWrap').animate({
				'height': '100%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': 'hidden',	
			});	
		} else {
			$('#cahtbotWrap').animate({
				'height': '80%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': '',	
			});	
			
			$('#btn_bot_sully').trigger('click');	
			$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);	
		}					
	});
	
	// joey open
	$('#cahtbot_rolling li.ico_joey a').on('click', function() {
		var winWidth = $(window).width();
		if ( winWidth < 760) {
			$('#cahtbotWrap').animate({
				'height': '100%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': 'hidden',	
			});		
		} else {
			$('#cahtbotWrap').animate({
				'height': '80%',	
				'opacity': '1.0',	
			},300);
			$('#maumWrap').css({
				'overflow': '',	
			});	
			
			$('#btn_bot_joey').trigger('click');	
			$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);	
		}	
	});
	
	// mini open
	$('#cahtbot_rolling li.ico_mini a').on('click', function() {	
		var winWidth = $(window).width();	
		if ( winWidth < 760) {
			$('#cahtbotWrap').animate({
				'height': '100%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': 'hidden',	
			});		
		} else {
			$('#cahtbotWrap').animate({
				'height': '80%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': '',	
			});	
			
			$('#btn_bot_mini').trigger('click');	
			$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);	
		}					
	});
	// noah open
	$('#cahtbot_rolling li.ico_noah a').on('click', function() {
		var winWidth = $(window).width();
		if ( winWidth < 760) {
			$('#cahtbotWrap').animate({
				'height': '100%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': 'hidden',	
			});		
		} else {
			$('#cahtbotWrap').animate({
				'height': '80%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': '',	
			});	
			
			$('#btn_bot_noah').trigger('click');	
			$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);	
		}					
	});
	
	// ttubot open
	$('#cahtbot_rolling li.ico_ttubot a').on('click', function() {
		var winWidth = $(window).width();
		if ( winWidth < 760) {
			$('#cahtbotWrap').animate({
				'height': '100%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': 'hidden',	
			});		
		} else {
			$('#cahtbotWrap').animate({
				'height': '80%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': '',	
			});	
			
			$('#btn_bot_ttubot').trigger('click');	
			$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);	
		}					
	});
	
	// top button
	$('.btn_goTop').on('click',function(){
		$('html, body').animate({scrollTop: 0}, 500);
	});	
	
	// chatbot list rolling
	/*
	$('#cahtbot_rolling').vTicker({
		// 스크롤 속도(default: 700)  
		speed: 800,  
		// 스크롤 사이의 대기시간(default: 4000)  
		pause: 5000,  
		// 스크롤 애니메이션  
		animation: 'fade',  
		// 마우스 over 일때 멈출 설정  
		mousePause: false,  
		// 한번에 보일 리스트수(default: 2)  
		showItems: 5,  
		// 스크롤 컨테이너 높이(default: 0)  
		height: 0,  
		// 아이템이 움직이는 방향, up/down (default: up)  
		direction: 'up'  
	}); 
    */
	// cahtbot nav active
	$('.cahtbot_nav ul li button').on('click', function(){
		$('.cahtbot_nav ul li button').removeClass('active'); 
		$(this).addClass('active'); 
	});
	
	// Layer popup close
	$('#btn_layerClose').on('click', function(){
		$('.layerPop').fadeOut(); 
	});
	
	// Layer popup open
	$('#btn_apply_open').on('click', function(){
		$('.layerPop').fadeIn(); 
		$('.applyBox .ipt_txt').each(function(){					
			$(this).val('');
		});
	});
	
	// apply
	$('#btn_apply').on('click', function(){
		$('.layerPop').fadeOut(); 
		alert('신청되었습니다.')
	});	
	
	$('.bg_aniBox').each(function(){
		var aniboxSize = $(this).parent().height();
		$(this).css({
			height: aniboxSize,
		}); //Hide all content
	});	
	
});

$(window).resize(function (){		
	// chatbot open
	$('#btn_flt_cb, #btn_chat_open').on('click', function() {
		var winWidth = $(window).width();
		if ( winWidth < 760) {
			$('#cahtbotWrap').animate({
				'height': '100%',	
				'opacity': '1.0',	
			},300);	
			$('#maumWrap').css({
				'overflow': 'hidden',	
			});		
		} else {
			$('#cahtbotWrap').animate({
				'height': '80%',	
				'opacity': '1.0',	
			},300);
			
			$('#maumWrap').css({
				'overflow': '',	
			});	
			
			//$('#btn_bot_sully').trigger('click');
			$('.chatbot_contents .chat_mid').scrollTop($('.chatbot_contents .chat_mid')[0].scrollHeight);	
		}
	});
});

// scroll event
$(window).scroll(function(){
	var st = $(window).scrollTop();
	
	if(st > 1) {	
		// wide image animation transform: matrix(1.08584, 0, 0, 1.08584, 0, -33.5788);
		$('.crop_cell .bg_aniBox img').css({transform: 'matrix(1.08584, 0, 0, 1.08584, 0,' + st * 0.0754});	
	} else {	
		// wide image animation
		$('.crop_cell .bg_aniBox img').css({transform: 'matrix(1.08584, 0, 0, 1.08584, 0,' - st * 0.0754});	
	}	

	$('#object01').each(function(){
		var winWidth = $(window).width();
		var objectOffset = $('#object01').offset().top;
		
		if ( winWidth < 760) {
			
			
			if(st > objectOffset - 1550) {
				$('#object01').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object01').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
			
			if(st > objectOffset - 1200) {
				$('#object02').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object02').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
			
			if(st > objectOffset - 850) {
				$('#object03').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object03').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}	
		} else {
			if(st > objectOffset - 800) {
				$('#object01').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object01').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
			
			if(st > objectOffset - 700) {
				$('#object02').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object02').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
			
			if(st > objectOffset - 620) {
				$('#object03').animate({
					'top':0,
					'opacity':1,
				},300);
			} else {
				$('#object03').animate({
					'top':'50px',
					'opacity':0,
				},300);
			}
		}
	});	

	
	
	
	

	
	
});

