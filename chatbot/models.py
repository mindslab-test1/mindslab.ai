from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField

# Create your models here.
class PR_Press(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(blank=True, upload_to='press')
    image_url = models.URLField(blank=True, null=True)
    content = RichTextField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)

class PR_Video(models.Model):
    title = models.CharField(max_length=200)
    video_url = models.URLField()
    category = models.CharField(max_length=50, blank=True, null=True)

class Recruitment(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(blank=True, upload_to='recruitment')     #(upload_to=upload_path, null=True, blank=True)
    content = RichTextField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
