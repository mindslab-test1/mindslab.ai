from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from . import views

urlpatterns = [
    path('', views.index, name='index'),

    # path('process_utter', views.process_utter, name='process_utter'),
    path('send_email', views.send_email, name='send_email'),
    # path('send_ebook_info', views.send_ebook_info, name='send_ebook_info'),
    # path('seminar_dwn', views.seminar_dwn, name='seminar_dwn'),

    # path('contact_us_page', views.contact_us_page, name='contact_us_page'),
    path('post_contact_us', views.post_contact_us, name='post_contact_us'),
    path('en', views.en_company, name='en_index'),
    path('en/company', views.en_company, name='en_company'),
    # path('en', views.en_acc, name='en_index'),
    # path('en/maum_chatbot', views.en_index, name='en_index'),
    # path('en/contact_us_page', views.en_contact_us_page, name='en_contact_us_page'),
    # path('en/acc', views.en_acc, name='en_acc'),
    # path('en/portfolio', views.en_portfolio, name='en_portfolio'),
    # path('en/floating_sully', views.en_floating_sully, name='en_floating_sully'),
    # path('en/pricing', views.en_pricing, name='en_pricing'),

    # path('kr', views.kr_acc, name='kr_index'),
    path('kr', views.kr_company, name='kr_index'),
    path('kr/company', views.kr_company, name='kr_company'),
    # path('kr/contact_us_page', views.kr_contact_us_page, name='kr_contact_us_page'),
    # path('kr/maum_chatbot', views.kr_maum_chatbot, name='kr_maum_chatbot'),
    # path('kr/acc', views.kr_acc, name='kr_acc'),
    # path('kr/portfolio', views.kr_portfolio, name='kr_portfolio'),
    # path('kr/floating_sully', views.kr_floating_sully, name='kr_floating_sully'),
    # path('kr/pricing', views.kr_pricing, name='kr_pricing'),
    # path('kr/product', views.kr_product, name='kr_product'),

    path('kr/press_list', views.kr_press_list, name="kr_press_list"),
    path('kr/video_list', views.kr_video_list, name="kr_video_list"),
    path('kr/press_detail', views.kr_press_detail, name="kr_press_detail"),
    path('kr/recruit_detail', views.kr_recruit_detail, name="kr_recruit_detail"),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
